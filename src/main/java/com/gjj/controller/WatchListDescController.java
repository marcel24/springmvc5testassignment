package com.gjj.controller;

import com.gjj.model.DataSet;
import com.gjj.service.accountService.AccountService;
import com.gjj.service.watchlist.WatchListDescServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

/**
 * Created by Simon Okwori on consc - 11/2/2017.
 */
@Controller
public class WatchListDescController {

    private static final String VIEWWATCHLIST_VIEW_NAME = "watchlist/view-watchlist";
    private static final String ADDWATCHLIST_VIEW_NAME = "watchlist/lazyRowLoad";

    @Autowired
    private WatchListDescServiceService service;
    @Autowired
    AccountService accountService;

    @GetMapping("/view-watchlist")
    public String viewAccount(Model model, @RequestParam Long id){
        model.addAttribute("stockSymbolsList", service.getStockSymbolsList(id));
        return VIEWWATCHLIST_VIEW_NAME;
    }

    @GetMapping("/add-watchlist")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String addWatchList(Principal principal, Model model){
        Assert.notNull(principal);
        DataSet dataSet = new DataSet();
        dataSet.setAccountId(accountService.getUserByEmail(principal.getName()).getId());
        model.addAttribute("theWatchListDesc", dataSet);
        return ADDWATCHLIST_VIEW_NAME;
    }

    @PostMapping("/lazyRowAdd")
    public String lazyRowAdd(@ModelAttribute("theWatchListDesc") DataSet dataSet){
        service.create(dataSet);
        return "redirect:/view-account?id="+dataSet.getAccountId();
    }

}
