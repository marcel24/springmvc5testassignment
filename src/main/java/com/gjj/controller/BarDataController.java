package com.gjj.controller;

import com.gjj.model.MarketData;
import com.gjj.service.barService.BarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Simon Okwori on consc - 11/2/2017.
 */
@Controller
public class BarDataController {

    private static final String VIEWDATA_VIEW_NAME = "bar/view-data";
    private static final String SEARCH_VIEW_NAME = "bar/search";

    @Autowired
    private BarService service;

    @GetMapping("/view-data")
    public String viewAccount(Model model, @RequestParam String stockSymbol){
        List<MarketData> dataList = service.getBarList(stockSymbol);
        model.addAttribute("barData", dataList);
        return VIEWDATA_VIEW_NAME;
    }

    @GetMapping("/search")
    public String searchGet1(){
        return SEARCH_VIEW_NAME;
    }

    @GetMapping("/DataController")
    public String searchGet2(Model model, @RequestParam String searchParam){
        List<String> tickets = service.searchTickersByChars(searchParam);
        model.addAttribute("THE_SEARCH_RESULT_LIST", tickets);
        return SEARCH_VIEW_NAME;
    }

    @PostMapping("/search")
    public String searchPost(Model model, @RequestParam String stockSymbol){
        List<MarketData> dataList = service.getBarList(stockSymbol);
        model.addAttribute("baaData", dataList);
        return VIEWDATA_VIEW_NAME;
    }
}
