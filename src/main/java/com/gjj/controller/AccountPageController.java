package com.gjj.controller;

import com.gjj.model.Account;
import com.gjj.service.accountService.AccountService;
import com.gjj.support.web.Ajax;
import com.gjj.support.web.MessageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

/**
 * Created by Simon Okwori on consc - 10/28/2017.
 */
@Controller
public class AccountPageController {

    private static final String LISTACCOUNT_VIEW_NAME = "account/list-accounts";
    private static final String EDITACCOUNT_VIEW_NAME = "account/editAccountForm";
    private static final String ADDACCOUNT_VIEW_NAME = "account/newAccountForm";
    private static final String DELETEACCOUNT_VIEW_NAME = "account/delete-account";

    @Autowired
    private AccountService accountService;


    @GetMapping("/list-accounts")
    @ResponseStatus(value = HttpStatus.OK)
    @Secured("ROLE_ADMIN")
    public String showAccounts(Model model){
        List<Account> accounts = accountService.getAll();
        model.addAttribute("accountList", accounts);
        return LISTACCOUNT_VIEW_NAME;
    }

    @GetMapping("/add-account")
    public String addAccount(Model model, @RequestHeader(value = "X-Requested-With", required = false) String requestedWith) {
        model.addAttribute("newAccount", new Account());
        if (Ajax.isAjaxRequest(requestedWith)) {
            return ADDACCOUNT_VIEW_NAME.concat(" :: addAccountForm");
        }
        return ADDACCOUNT_VIEW_NAME;
    }

    @PostMapping("/add-account")
    public String addAccount2(@Valid @ModelAttribute Account account, Errors errors, RedirectAttributes ra) {
        if (errors.hasErrors()) {
            return ADDACCOUNT_VIEW_NAME;
        }
        account.setEnabled(true); account.setCreationDate(Instant.now());
        accountService.save(account);
        MessageHelper.addSuccessAttribute(ra, "addAccount.success");
        return "redirect:/list-accounts";
    }

    @GetMapping("/edit-account")
    public String editAccount( Model model, @RequestHeader(value = "X-Requested-With", required = false) String requestedWith , @RequestParam int id) {
        Account account = accountService.getAccount((long) id);
        model.addAttribute("editAccount", account);
        if (Ajax.isAjaxRequest(requestedWith)) {
            return EDITACCOUNT_VIEW_NAME.concat(" :: editAccountForm");
        }
        return EDITACCOUNT_VIEW_NAME;
    }

    @PostMapping("/edit-account")
    public String editAccount2(@ModelAttribute Account account, Errors errors, RedirectAttributes ra) {
        if (errors.hasErrors()) {
            return EDITACCOUNT_VIEW_NAME;
        }
        Account account1 = accountService.getAccount(account.getId());
        account1.setName(account.getName());
        account1.setEmail(account.getEmail());
        account1.setAdditionalInfo(account.getAdditionalInfo());
        accountService.updateAccount(account1);
        MessageHelper.addSuccessAttribute(ra, "editAccount.success");
        return "redirect:/list-accounts";
    }

    @GetMapping("/delete-account")
    public String deleteAccount( Model model, @RequestHeader(value = "X-Requested-With", required = false) String requestedWith , @RequestParam int id) {
        Account account = accountService.getAccount((long) id);
        model.addAttribute("delAccount", account);
        if (Ajax.isAjaxRequest(requestedWith)) {
            return DELETEACCOUNT_VIEW_NAME.concat(" :: delAccountForm");
        }
        return DELETEACCOUNT_VIEW_NAME;
    }

    @PostMapping("/delete-account")
    public String deleteAccount2(@ModelAttribute Account account, Errors errors, RedirectAttributes ra) {
        if (errors.hasErrors()) {
            return DELETEACCOUNT_VIEW_NAME;
        }
        Account account1 = accountService.getAccount(account.getId());
        account1.setEnabled(false);
        accountService.updateAccount(account1);
        MessageHelper.addSuccessAttribute(ra, "delAccount.success");
        return "redirect:/list-accounts";
    }
}
