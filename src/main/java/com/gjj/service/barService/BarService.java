package com.gjj.service.barService;

import com.gjj.dao.BarDao;
import com.gjj.model.MarketData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Simon Okwori on consc - 11/2/2017.
 */
@Service
public class BarService {

    @Autowired
    private BarDao dataDao;

    public List<MarketData> getBarList(String instId){
        return dataDao.findAllByInstId(instId);
    }

    public MarketData getSingleBar(Long mdId, String instId){
        return dataDao.findByIdAndInstId(mdId, instId);
    }

    public void createBar(MarketData marketData){
        dataDao.save(marketData);
    }

    public MarketData getOne(Long id){
        return dataDao.getOne(id);
    }

    public void deleteBar(MarketData marketData){
        dataDao.delete(marketData);
    }

    public void deleteBar(Long id){
        dataDao.delete(id);
    }

    public List<String> searchTickersByChars(String instId){
        return dataDao.searchTickersByChars(instId);
    }
}
