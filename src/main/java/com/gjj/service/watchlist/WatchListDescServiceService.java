package com.gjj.service.watchlist;

import com.gjj.dao.DataSetDao;
import com.gjj.dao.WlDao;
import com.gjj.model.DataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Simon Okwori on consc - 11/2/2017.
 */
@Service
public class WatchListDescServiceService {

    @Autowired
    private DataSetDao dataSetDao;

    @Autowired
    WlDao wlDao;

    public List<DataSet> getDataSetsAttachedToAcc(Long accId){
        return dataSetDao.findAllByAccountId(accId);
    }

    public List<String> getStockSymbolsList(Long id){
        return wlDao.getAllStockSymbols(id);
    }

    @Transactional
    public boolean delete(Long id){
        boolean flag;
        try{
            dataSetDao.delete(id);
            flag = true;
        }catch (Exception e){
            flag = false;
        }
        return flag;
    }

    public boolean delete(DataSet dataSet){
        boolean flag;
        try{
            dataSetDao.delete(dataSet);
            flag = true;
        }catch (Exception e){
            flag = false;
        }
        return flag;
    }

    @Transactional
    public boolean create(DataSet dataSet){
        boolean flag;
        try{
            dataSetDao.save(dataSet);
            flag = true;
        }catch (Exception e){
            flag = false;
        }
        return flag;
    }

    public DataSet getWatchListDesc(Long id){
        return dataSetDao.getOne(id);
    }

}
