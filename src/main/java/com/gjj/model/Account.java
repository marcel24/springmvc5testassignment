package com.gjj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@SuppressWarnings("serial")
@Entity
@Table(name = "account")
public class Account implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "account_name", nullable = false)
	private String name;

	@Column(unique = true, nullable = false)
	private String email;

	@Column(name = "additional_info")
	private String additionalInfo;

	@JsonIgnore
	@Column(name = "password", nullable = false)
	private String password;

	private String role = "ROLE_USER";

	@Column(name = "creation_date")
	private Instant creationDate;

	@Column(name = "image")
	private byte[] image;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	public Account() {}

	public Account(String email, String password, String role, String name, boolean enabled) {
		this.email = email;
		this.password = password;
		this.role = role;
		this.creationDate = Instant.now();
		this.name = name;
		this.enabled = true;
	}

	public Account(String name, String email, String additionalInfo, String password, String role, Instant creationDate, byte[] image, boolean enabled) {
		this.name = name;
		this.email = email;
		this.additionalInfo = additionalInfo;
		this.password = password;
		this.role = role;
		this.creationDate = Instant.now();
		this.image = image;
		this.enabled = enabled;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Instant getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Instant creationDate) {
		this.creationDate = creationDate;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date convertDate(){
		return Date.from(getCreationDate());
	}

}
