package com.gjj.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Simon Okwori on consc - 11/2/2017.
 */
@Entity
@Table(name = "wl_tickers")
public class WlTickers implements Serializable{

    @Id
    @ManyToOne
    @JoinColumn(name = "data_set_id")
    private DataSet dataSet;

    @Id
    @ManyToOne
    @JoinColumn(name = "md_id")
    private MarketData marketData;

    @Column(name = "inst_id")
    private String instId;

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public MarketData getMarketData() {
        return marketData;
    }

    public void setMarketData(MarketData marketData) {
        this.marketData = marketData;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }
}
