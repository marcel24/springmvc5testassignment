package com.gjj.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Simon Okwori on consc - 10/22/2017.
 */
@Entity
@Table(name = "data_set")
public class DataSet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "data_set_id")
    private Long id;

    @Column(name = "account_fk_id", nullable = false)
    private Long accountId;

    @Column(name = "data_set_name", nullable = false)
    private String name;

    @Column(name = "data_set_description")
    private String description;

    @Column(name = "market_data_frequency")
    private Integer marketDataFrequency;

    @Column(name = "data_providers")
    private String dataProviders;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_fk_id", nullable = false, insertable = false, updatable = false)
    private Account account;

    @OneToMany(mappedBy = "dataSet", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WlTickers> wlTickers;

    public DataSet() {
    }

    public DataSet(Long accountId, String name, String description, Integer marketDataFrequency, String dataProviders, Account account) {
        this.accountId = accountId;
        this.name = name;
        this.description = description;
        this.marketDataFrequency = marketDataFrequency;
        this.dataProviders = dataProviders;
        this.account = account;
    }

    public DataSet(Long accountId, String name, String description, Integer marketDataFrequency, String dataProviders, Account account, Set<WlTickers> wlTickers) {
        this.accountId = accountId;
        this.name = name;
        this.description = description;
        this.marketDataFrequency = marketDataFrequency;
        this.dataProviders = dataProviders;
        this.account = account;
        this.wlTickers = wlTickers;
    }

    public DataSet(Set<WlTickers> wlTickers) {
        this.wlTickers = wlTickers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMarketDataFrequency() {
        return marketDataFrequency;
    }

    public void setMarketDataFrequency(Integer marketDataFrequency) {
        this.marketDataFrequency = marketDataFrequency;
    }

    public String getDataProviders() {
        return dataProviders;
    }

    public void setDataProviders(String dataProviders) {
        this.dataProviders = dataProviders;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<WlTickers> getWlTickers() {
        return wlTickers;
    }

    public void setWlTickers(Set<WlTickers> wlTickers) {
        this.wlTickers = wlTickers;
    }
}
