package com.gjj.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Simon Okwori on consc - 10/22/2017.
 */
@Entity
@Table(name = "market_data")
public class MarketData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "md_id")
    private Long id;

    @Column(name = "instId_fk", nullable = false)
    private String instId;

    @Column(name = "ticker", nullable = false)
    private String ticker;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "open", nullable = false)
    private Double open;

    @Column(name = "high", nullable = false)
    private Double high;

    @Column(name = "low", nullable = false)
    private Double low;

    @Column(name = "close", nullable = false)
    private Double close;

    @Column(name = "vol", nullable = false)
    private Integer vol;

    @Column(name = "additional_info")
    private String additionalInfo;

    @OneToMany(mappedBy = "marketData")
    private Set<WlTickers> wlTickers;

    public MarketData() {
    }

    public MarketData(String instId, String ticker, Date date, Double open, Double high, Double low, Double close, Integer vol) {
        this.instId = instId;
        this.ticker = ticker;
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.vol = vol;
    }

    public MarketData(Set<WlTickers> wlTickers) {
        this.wlTickers = wlTickers;
    }

    public MarketData(String instId, String ticker, Date date, Double open, Double high, Double low, Double close, Integer vol, Set<WlTickers> wlTickers) {
        this.instId = instId;
        this.ticker = ticker;
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.vol = vol;
        this.wlTickers = wlTickers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Integer getVol() {
        return vol;
    }

    public void setVol(Integer vol) {
        this.vol = vol;
    }

    public Set<WlTickers> getWlTickers() {
        return wlTickers;
    }

    public void setWlTickers(Set<WlTickers> wlTickers) {
        this.wlTickers = wlTickers;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
