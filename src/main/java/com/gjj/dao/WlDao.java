package com.gjj.dao;

import com.gjj.model.WlTickers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Simon Okwori on consc - 11/2/2017.
 */
@Repository
public interface WlDao extends JpaRepository<WlTickers, Long> {

    @Query(value = "SELECT inst_id FROM wl_tickers WHERE data_set_id = ?1", nativeQuery = true)
    List<String> getAllStockSymbols(Long dataSetId);
}
