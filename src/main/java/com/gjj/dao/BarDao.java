package com.gjj.dao;

import com.gjj.model.MarketData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Simon Okwori on consc - 10/22/2017.
 */
@Repository
public interface BarDao extends JpaRepository<MarketData, Long> {

    MarketData findByIdAndInstId(Long mdId, String instId);

    List<MarketData> findAllByInstId(String s);

    @Query(value = "SELECT DISTINCT instId_fk FROM market_data WHERE instId_fk LIKE CONCAT(?1, '%')", nativeQuery = true)
    List<String> searchTickersByChars(String instId);

}
