package com.gjj.dao;

import com.gjj.model.DataSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Simon Okwori on consc - 10/22/2017.
 */
@Repository
public interface DataSetDao extends JpaRepository<DataSet, Long> {

//    TODO getAllStockSymbols(int watchListDescId)

    List<DataSet> findAllByAccountId(Long accId);

    DataSet findByIdAndAccountId(Long id, Long accId);

}
